# Документация за проекта може да бъде намерена във файла ProjectTempplate.docx 

## Project Description

The Project consists of arduino with sensors for light, humidity and water pump, 
backend server written on Spring Boot and deployed in Heroku. Also an Android app
which displays the data gathered from the sensors and can update the humidity threshold.